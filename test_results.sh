#!bin/bash

echo "What app do you want to run?"

read FOLDER
FILE=$(echo $FOLDER | tr " " "_" | tr /[A-Z]/ /[a-z]/)
PATH="./${FOLDER}/${FILE}"

PY_RES=""
JS_RES=""

if [ -f "${PATH}.py" ] && [ -f "${PATH}.js" ]; then
    $PY_RES=$(python "${PATH}.py")
    $JS_RES=$(node "${PATH}.js")
else
    echo "Either one or more of your files are missing, check your projct integrity or provide another input"
    exit 1
fi


if [ $PY_RES == $JS_RES ]; then
    echo "The scrips provide the same results!"
else
    echo "The scrips does NOT provide the same results!"
    exit 1
fi


STATUS=$?
if [ $STATUS != 0 ]; then
    echo "Error code $STATUS"
    exit 1
fi

echo "Done"
