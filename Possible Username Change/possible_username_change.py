def possibleChanges(usernames):
    lowUsernames = [username.lower() for username in usernames]

    def check(username):
        firstLetter = username[0]
        for letter in username:
            if letter < firstLetter:
                return 'YES'
        return 'NO'

    res = [check(username) for username in lowUsernames]

    return res


print(possibleChanges(["bee", "superhero", "ace"]))

