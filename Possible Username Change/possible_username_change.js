const possibleChanges = (usernames) => {
    const lowUsernames = usernames.map(name => name.toLowerCase())

    const res = lowUsernames.map(username => {
        const firstLetter = username[0][0]
        for (let index = 0; index < username.length; index++) {
            if (username[index] < firstLetter) {
                return 'YES'
            }
        }

        return 'NO'
    })

    return res
}

console.log(possibleChanges(["bee", "superhero", "ace"]));
