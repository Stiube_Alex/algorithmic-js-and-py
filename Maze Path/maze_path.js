const maze = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1, 2, 0, 1, 0, 0],
    [0, 1, 1, 0, 1, 1, 1, 1, 0, 0],
    [0, 0, 1, 1, 1, 1, 0, 1, 0, 0],
    [0, 1, 1, 1, 1, 0, 0, 1, 0, 0],
    [0, 3, 1, 0, 0, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const visited_maze = Array.from(Array(7), () => Array.from({ length: 10 }, () => false));

class Position {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

const navigate = (position, map, visited, res) => {
    res.push(position);

    if (map[position.x][position.y] === 3) {
        return res;
    }
    visited[position.x][position.y] = true;

    // North
    if (map[position.x][position.y + 1] && !visited[position.x][position.y + 1]) {
        return navigate(new Position(position.x, position.y + 1), map, visited, res);
    }

    // East
    if (map[position.x + 1][position.y] && !visited[position.x + 1][position.y]) {
        return navigate(new Position(position.x + 1, position.y), map, visited, res);
    }

    // South
    if (map[position.x][position.y - 1] && !visited[position.x][position.y - 1]) {
        return navigate(new Position(position.x, position.y - 1), map, visited, res);
    }

    // West
    if (map[position.x - 1][position.y] && !visited[position.x - 1][position.y]) {
        return navigate(new Position(position.x - 1, position.y), map, visited, res);
    }

    const oldPosition = res[res.length - 2];
    res.pop();
    res.pop();

    return navigate(oldPosition, map, visited, res);
};

const print = (positionsArray) => {
    return positionsArray.map((position) => {
        return `( ${position.x} , ${position.y} )`;
    });
};

const path = navigate(new Position(1, 5), maze, visited_maze, [])
console.log(print(path));

