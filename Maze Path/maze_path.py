maze = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1, 2, 0, 1, 0, 0],
    [0, 1, 1, 0, 1, 1, 1, 1, 0, 0],
    [0, 0, 1, 1, 1, 1, 0, 1, 0, 0],
    [0, 1, 1, 1, 1, 0, 0, 1, 0, 0],
    [0, 3, 1, 0, 0, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]


visited_maze = []
for i in range(7):
    visited_maze.append([])
    for j in range(7):
        visited_maze[i].append(False)


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return f"( {self.x} , {self.y} )"


def navigate(position, map, visited, res):
    res.append(position)

    if (map[position.x][position.y] == 3):
        return res

    visited[position.x][position.y] = True

    # North
    if (map[position.x][position.y + 1] and not(visited[position.x][position.y + 1])):
        return navigate(Position(position.x, position.y + 1), map, visited, res)

    # East
    if (map[position.x + 1][position.y] and not(visited[position.x + 1][position.y])):
        return navigate(Position(position.x + 1, position.y), map, visited, res)

    # South
    if (map[position.x][position.y - 1] and not(visited[position.x][position.y - 1])):
        return navigate(Position(position.x, position.y - 1), map, visited, res)

    # West
    if (map[position.x - 1][position.y] and not(visited[position.x - 1][position.y])):
        return navigate(Position(position.x - 1, position.y), map, visited, res)

    oldPosition = res[len(res) - 2]
    res.pop()
    res.pop()

    return navigate(oldPosition, map, visited, res)


def show(positionsArray):
    printed = []
    for position in positionsArray:
        printed.append(f"( {position.x} , {position.y} )")
    return printed


path = navigate(Position(1, 5), maze, visited_maze, [])
print(show(path))

