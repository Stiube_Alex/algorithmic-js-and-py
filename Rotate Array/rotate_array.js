const rotateArray = (times, arr, direction = -1) => {
    try {
        if (times === 0) {
            return arr
        }

        if (direction !== 1 && direction !== -1) {
            throw new Error('Invalid direction')
        }

        if (direction === -1) {
            arr.push(arr.shift())
        }

        if (direction === 1) {
            arr.unshift(arr.pop())
        }

        times--
        return rotateArray(times, arr, direction)
    } catch (error) {
        return error
    }
}

console.log(rotateArray(4, [1, 2, 3, 4, 5], 1));


