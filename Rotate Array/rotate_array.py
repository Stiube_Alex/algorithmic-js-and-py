def rotateArray(times, arr, direction=-1):
    if times == 0:
        return arr

    if (direction != 1 and direction != -1):
        raise Exception('Invalid direction')

    if direction == -1:
        arr.append(arr.pop(0))

    if (direction == 1):
        arr.insert(0, arr.pop(len(arr)-1))

    times -= 1
    return rotateArray(times, arr, direction)


print(rotateArray(4, [1, 2, 3, 4, 5], 1))

