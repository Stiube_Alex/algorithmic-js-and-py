def super_digit(n, k):

    def inner(n, k):
        curentN = str(n)

        if len(curentN) == 1:
            return n

        if k > 0:
            repeated = ''
            for item in range(0, k):
                repeated += curentN
            k = 0
            curentN = repeated

        lastDigit = int(curentN[len(curentN) - 1])
        numN = curentN[0:len(curentN)-1]

        return lastDigit + inner(int(numN), k)

    current = inner(n, k)
    if len(str(current)) > 1:
        return super_digit(current, 0)

    return current


print(super_digit('9875', 4))

