const super_digit = (n, k) => {

    const inner = (n, k) => {
        if (n.length === 1) {
            return parseInt(n)
        }

        if (k > 0) {
            let repeated = '';
            for (let index = 0; index < k; index++) {
                repeated += n
            }
            k = 0
            n = repeated
        }

        let lastDigit = parseInt(n[n.length - 1])
        let newNum = n.slice(0, n.length - 1)

        return lastDigit + inner(newNum, k)
    }

    const current = inner(n, k)
    if (current.toString().length > 1) {
        return super_digit(current.toString(), 0)
    }

    return parseInt(current)
}

console.log(super_digit('9875', 4));
